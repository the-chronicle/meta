import { config } from "https://deno.land/x/dotenv@v3.0.0/mod.ts";

export const env = {...config({ safe: true }), ...Deno.env.toObject()}