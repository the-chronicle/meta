export default function toArticleUrl({ date, headline, prefix = '', suffix = '' }: { date: Date, headline: string, prefix?: string, suffix?: string }) {
  return `${prefix}/${date.toLocaleDateString('en', {
    month: '2-digit',
    day: '2-digit',
    year: '2-digit'
  })}/${headline.replace(/[ ?!]/g, '-').replace(/-$/, '')}${suffix}`;
}