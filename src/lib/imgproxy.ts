/*

Source: https://github.com/progapandist/imgproxy-form/blob/gh-pages/index.js
Author: And[re]y Bar[a]nov 

*/

import jsSHA from 'https://esm.sh/jssha@3.2.0'
import { env } from './env.ts'

// Helper for URL generator
function hex2a(hexx: string) {
  const hex = hexx.toString() //force conversion
  let str = ''
  for (let i = 0; i < hex.length; i += 2)
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
  return str
}

interface Options {
  url: string
  resize: string
  width: number
  height: number
  enlarge: number
  gravity: string
  extension: string
  key: string
  salt: string
  proxyUrl: string
}

// URL generator logic
function generateProxyUrl(opts: Options) {
  const encodedUrl = btoa(opts.url)
    .replace(/=/g, '')
    .replace(/\//g, '_')
    .replace(/\+/g, '-')
  const path =
    '/rs:' +
    opts.resize +
    ':' +
    opts.width +
    ':' +
    opts.height +
    ':' +
    opts.enlarge +
    '/g:' +
    opts.gravity +
    '/' +
    encodedUrl +
    '.' +
    opts.extension
  const shaObj = new jsSHA('SHA-256', 'BYTES')
  shaObj.setHMACKey(opts.key, 'HEX')
  shaObj.update(hex2a(opts.salt))
  shaObj.update(path)
  const hmac = shaObj
    .getHMAC('B64')
    .replace(/=/g, '')
    .replace(/\//g, '_')
    .replace(/\+/g, '-')
  return opts.proxyUrl + '/' + hmac + path
}

export function imgproxyUrl(url: string) {
  return generateProxyUrl({
    url,
    resize: 'fit',
    width: 900,
    height: 900,
    enlarge: 0,
    gravity: 'no',
    extension: 'webp',
    proxyUrl: env['IMGPROXY_URL'],
    key: env['IMGPROXY_KEY'],
    salt: env['IMGPROXY_SALT'],
  })
}
