import { download } from "https://deno.land/x/download@v1.0.1/mod.ts";
import { tar } from "https://deno.land/x/compress@v0.3.0/mod.ts";
import { emptyDir } from 'https://deno.land/std@0.109.0/fs/mod.ts'

export default async function (): Promise<void> {
  await emptyDir('./download')

  await download('https://gitlab.com/pasadena-chronicle/newspaper/-/archive/main/newspaper-main.tar', {
    file: 'data.tar',
    dir: './download'
  })
  console.log('Downloaded data.tar')

  await tar.uncompress("./download/data.tar", "./download")
  console.log('Uncompressed data.tar')
}