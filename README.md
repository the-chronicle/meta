### This repository has been archived

The meta server is made obsolete by the new version of the website.

# meta

This repository JSON-ifies the data from the newspaper repository and publishes it as a usable API hosted on GitLab pages.
